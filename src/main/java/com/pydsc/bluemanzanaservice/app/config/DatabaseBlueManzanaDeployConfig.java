package com.pydsc.bluemanzanaservice.app.config;

import com.pydsc.bluemanzanaservice.app.properties.DatabaseConfigProperties;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = {
                "com.pydsc.bluemanzanaservice.modules.shared.infrastructure.out.db.postgres",
                "com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres",
                "com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres",
                "com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres",
                "com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres"
        },
        entityManagerFactoryRef = "blueManzanaEntityManager",
        transactionManagerRef = "blueManzanaTransactionManager")
public class DatabaseBlueManzanaDeployConfig {

    @Bean
    @ConfigurationProperties("pydsc.bluemanzanaservice.datasource")
    @Primary
    public DataSourceProperties blueManzanaDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource blueManzanaDataSource() {
        return blueManzanaDataSourceProperties().initializeDataSourceBuilder().build();
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean blueManzanaEntityManager() {

        final var em = new LocalContainerEntityManagerFactoryBean();
        em.setJpaVendorAdapter(vendorAdaptor());
        em.setDataSource(blueManzanaDataSource());
        em.setPersistenceUnitName("blueManzana");
        em.setPackagesToScan(
                DatabaseConfigProperties.SHARED_MODULE_DB_POSTGRES,
                DatabaseConfigProperties.PRODUCTOS_MODULE_DB_POSTGRES,
        		DatabaseConfigProperties.TIENDAS_MODULE_DB_POSTGRES,
        		DatabaseConfigProperties.USUARIOS_MODULE_DB_POSTGRES,
				DatabaseConfigProperties.PEDIDOS_MODULE_DB_POSTGRES);
        em.afterPropertiesSet();
        return em;
    }

    @Bean
    @Primary
    public PlatformTransactionManager blueManzanaTransactionManager() {

        final var transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(blueManzanaEntityManager().getObject());
        return transactionManager;
    }

    private HibernateJpaVendorAdapter vendorAdaptor() {

        final var vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
        return vendorAdapter;
    }
}
