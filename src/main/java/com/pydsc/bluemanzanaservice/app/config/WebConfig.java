package com.pydsc.bluemanzanaservice.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {

        registry.addMapping("/**")
                .allowedOriginPatterns(CorsConfiguration.ALL)
                .allowedMethods("HEAD", "GET", "POST", "PATCH", "DELETE", "PUT", "OPTIONS")
                .allowedHeaders("Authorization", "Cache-Control", "Content-Type", "ua-version")
                .allowCredentials(true)
                .maxAge(32400);
    }
}
