package com.pydsc.bluemanzanaservice.app.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.pydsc.bluemanzanaservice.modules.usuarios.application.finder.AuthenticationFilter;
import com.pydsc.bluemanzanaservice.modules.usuarios.application.finder.LoginFilter;
import com.pydsc.bluemanzanaservice.modules.usuarios.application.finder.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	  
	private UserDetailsServiceImpl userDetailsService;  

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and().authorizeRequests()  
        .antMatchers(HttpMethod.POST, "/login").permitAll()  
        .anyRequest().authenticated().and()  
        .addFilterBefore(new LoginFilter("/login", authenticationManager()),  
                UsernamePasswordAuthenticationFilter.class)  
        .addFilterBefore(new AuthenticationFilter(),  
                UsernamePasswordAuthenticationFilter.class);
	} 
	  
	@Bean  
	CorsConfigurationSource corsConfigurationSource() {  
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();  
		CorsConfiguration config = new CorsConfiguration();  
		config.setAllowedOriginPatterns(Arrays.asList("*"));  
		config.setAllowedMethods(Arrays.asList("*"));  
		config.setAllowedHeaders(Arrays.asList("*"));  
		config.setAllowCredentials(true);  
		config.applyPermitDefaultValues();  
		  
		source.registerCorsConfiguration("/**", config);  
		return source;  
    }  

	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
		return authenticationManager();
	}

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}

}