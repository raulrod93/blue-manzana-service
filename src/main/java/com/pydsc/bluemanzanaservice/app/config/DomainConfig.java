package com.pydsc.bluemanzanaservice.app.config;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Service;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@ComponentScan(
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Service.class),
        value = {
                "com.pydsc.bluemanzanaservice.modules.shared.domain",
                "com.pydsc.bluemanzanaservice.modules.productos.domain",
                "com.pydsc.bluemanzanaservice.modules.tiendas.domain",
                "com.pydsc.bluemanzanaservice.modules.usuarios.domain",
                "com.pydsc.bluemanzanaservice.modules.pedidos.domain"
        })
@Configuration
public class DomainConfig {
    // DomainConfig
}
