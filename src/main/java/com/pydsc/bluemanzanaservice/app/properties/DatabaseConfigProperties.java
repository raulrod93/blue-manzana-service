package com.pydsc.bluemanzanaservice.app.properties;

public final class DatabaseConfigProperties {

    private DatabaseConfigProperties() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static final String SHARED_MODULE_DB_POSTGRES = "com.pydsc.bluemanzanaservice.modules.shared.infrastructure.out.db.postgres";
    public static final String PRODUCTOS_MODULE_DB_POSTGRES = "com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres";
    public static final String TIENDAS_MODULE_DB_POSTGRES = "com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres";
    public static final String USUARIOS_MODULE_DB_POSTGRES = "com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres";
    public static final String PEDIDOS_MODULE_DB_POSTGRES = "com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres";


}
