package com.pydsc.bluemanzanaservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.XADataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, XADataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {
        "com.pydsc.bluemanzanaservice.app",
        "com.pydsc.bluemanzanaservice.modules.productos",
        "com.pydsc.bluemanzanaservice.modules.tiendas",
        "com.pydsc.bluemanzanaservice.modules.pedidos",
        "com.pydsc.bluemanzanaservice.modules.usuarios",
        "com.pydsc.bluemanzanaservice.modules.shared"})
public class BlueManzanaServiceApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BlueManzanaServiceApplication.class, args);
	}

}
