package com.pydsc.bluemanzanaservice.modules.usuarios.domain;

public class UsuarioNotFoundException extends RuntimeException {

    public UsuarioNotFoundException(String message) {
        super(message);
    }

    public static UsuarioNotFoundException ofId(final String id) {
        return new UsuarioNotFoundException("Usuario con [id] " + id + " no encontrado");
    }
}
