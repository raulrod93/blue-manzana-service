package com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresJpaUsuariosRepository extends JpaRepository<UsuarioEntity, Short> {
    public UsuarioEntity findByDni(String dni);

}
