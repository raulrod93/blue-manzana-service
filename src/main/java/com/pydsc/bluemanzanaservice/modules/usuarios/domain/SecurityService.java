package com.pydsc.bluemanzanaservice.modules.usuarios.domain;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String dni, String contrasenia);
}