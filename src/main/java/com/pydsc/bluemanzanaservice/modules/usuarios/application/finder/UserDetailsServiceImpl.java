package com.pydsc.bluemanzanaservice.modules.usuarios.application.finder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pydsc.bluemanzanaservice.modules.usuarios.domain.UsuarioNotFoundException;
import com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres.PostgresJpaUsuariosRepository;
import com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres.UsuarioEntity;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private PostgresJpaUsuariosRepository repository;
    
    public User findUsuarioByDni(String dni) throws UsuarioNotFoundException {
    	 
    	UsuarioEntity user = repository.findByDni(dni);

        if (user == null) {
            throw new UsuarioNotFoundException(dni);
        }
        
        return new org.springframework.security.core.userdetails.User(user.getDni(), user.getPassword(),    
                Arrays.asList(new SimpleGrantedAuthority("user")));  
 
    }

    @Override
    @Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String dni) throws UsernameNotFoundException {
		UsuarioEntity user = repository.findByDni(dni);
        if (user == null) throw new UsernameNotFoundException(dni);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        return new org.springframework.security.core.userdetails.User(user.getDni(), user.getPassword(), grantedAuthorities);
    }
}
