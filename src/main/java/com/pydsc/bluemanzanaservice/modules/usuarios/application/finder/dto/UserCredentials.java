package com.pydsc.bluemanzanaservice.modules.usuarios.application.finder.dto;

import lombok.Getter;  
import lombok.Setter;  
  
@Getter  
@Setter  
public class UserCredentials {  
    private String username;  
    private String password;  
}