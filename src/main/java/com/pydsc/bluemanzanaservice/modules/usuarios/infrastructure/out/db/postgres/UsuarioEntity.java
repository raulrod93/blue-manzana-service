package com.pydsc.bluemanzanaservice.modules.usuarios.infrastructure.out.db.postgres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.EntityConstants;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = EntityConstants.Tables.USUARIOS)
public class UsuarioEntity {

    @Id
    @Column(name = "dni")
    private String dni;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "apellidos", nullable = false)
    private String apellidos;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "telefono", nullable = false)
    private Short telefono;

    @Override
    public String toString() {
        return "UsuarioEntity{";
        // TODO
    }

}
