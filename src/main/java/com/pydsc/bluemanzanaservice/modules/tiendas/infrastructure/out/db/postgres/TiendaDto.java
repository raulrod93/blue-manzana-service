package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class TiendaDto {
	
	private final Short id;
	
	private final Short telefono;
	
	private final DireccionDto direccion;
	
	private final byte[] imagen;
}
