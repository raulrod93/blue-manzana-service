package com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ExistenciasEnTiendaFilterQuery {

    private String productoId;

}
