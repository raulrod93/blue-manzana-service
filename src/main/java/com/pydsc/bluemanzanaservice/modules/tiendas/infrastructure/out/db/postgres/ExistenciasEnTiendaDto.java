package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres.ProductoDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ExistenciasEnTiendaDto {
	
	private final Short cantidad;
	
	private final ProductoDto producto;
	
	private final TiendaDto tienda;
}
