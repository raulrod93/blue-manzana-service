package com.pydsc.bluemanzanaservice.modules.tiendas.application.filter;

import java.util.stream.Collectors;

import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQuery;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaCriteria;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaResponse;

public class ExistenciasEnTiendaFilterMapper {

    private ExistenciasEnTiendaFilterMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }


    public static ExistenciasEnTiendaCriteria queryToExistenciasEnTiendaCriteria(final ExistenciasEnTiendaFilterQuery query) {
        return ExistenciasEnTiendaCriteria.newBuilder()
        		.productoId(query.getProductoId())
                .build();
    }

    static ExistenciasEnTiendaFilterQueryResponse productRecordToQueryResponse(
            final ExistenciasEnTiendaCriteriaResponse existenciasEnTiendaCriteriaResponse) {

        final var queryFilter = criteriaToQuery(existenciasEnTiendaCriteriaResponse.getFilter());

        final var queryRecords = existenciasEnTiendaCriteriaResponse.getRecords().stream()
                .map(ExistenciasEnTiendaFilterMapper::existenciasEnTiendaRecordToResponse).collect(Collectors.toList());

        return new ExistenciasEnTiendaFilterQueryResponse(queryFilter, queryRecords);
    }

    public static ExistenciasEnTiendaResponse productRecordToResponse(final ExistenciasEnTiendaRecord existenciasEnTienda) {

        final var output = new ExistenciasEnTiendaResponse();

        output.setCantidad(existenciasEnTienda.getCantidad());
        output.setTienda(existenciasEnTienda.getTienda());
        output.setProducto(existenciasEnTienda.getProducto());

        return output;
    }
    static ExistenciasEnTiendaFilterRecord existenciasEnTiendaRecordToResponse(final ExistenciasEnTiendaRecord existenciasEnTiendaRecord) {

        return baseBuilder(existenciasEnTiendaRecord).build();
    }


    static ExistenciasEnTiendaFilterQuery criteriaToQuery(final ExistenciasEnTiendaCriteria criteria) {
        return ExistenciasEnTiendaFilterQuery.builder()
        		.productoId(criteria.getProductoId())
                .build();
    }

    private static ExistenciasEnTiendaFilterRecord.Builder baseBuilder(final ExistenciasEnTiendaRecord existenciasEnTiendaRecord) {

        return ExistenciasEnTiendaFilterRecord.builder()
        		.cantidad(existenciasEnTiendaRecord.getCantidad())
        		.tienda(existenciasEnTiendaRecord.getTienda())
        		.producto(existenciasEnTiendaRecord.getProducto());
    }

}
