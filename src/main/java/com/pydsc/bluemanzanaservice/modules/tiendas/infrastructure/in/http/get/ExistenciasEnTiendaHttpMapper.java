package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get;

import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQuery;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto.ExistenciasEnTiendaFilterHttpResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto.ExistenciasEnTiendaHttpFilter;
import com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto.ExistenciasEnTiendaHttpRecord;

import java.util.stream.Collectors;

final class ExistenciasEnTiendaHttpMapper {

    private ExistenciasEnTiendaHttpMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static ExistenciasEnTiendaHttpFilter queryFilterToTHttpFilter(final ExistenciasEnTiendaFilterQuery filter) {

        return ExistenciasEnTiendaHttpFilter.builder()
                .productoId(filter.getProductoId())
                .build();
    }

    static ExistenciasEnTiendaHttpRecord queryRecordToHttpRecord(final ExistenciasEnTiendaFilterRecord existenciasEnTiendaFilterRecord) {

        return ExistenciasEnTiendaHttpRecord.builder()
        		.cantidad(existenciasEnTiendaFilterRecord.getCantidad())
        		.tienda(existenciasEnTiendaFilterRecord.getTienda())
        		.producto(existenciasEnTiendaFilterRecord.getProducto())
                .build();
    }

    static ExistenciasEnTiendaFilterHttpResponse queryResponseToHttpResponse(
            final ExistenciasEnTiendaFilterQueryResponse response) {

        final var httpFilter = queryFilterToTHttpFilter(response.getFilter());

        final var httpRecords = response.getRecords().stream()
                .map(ExistenciasEnTiendaHttpMapper::queryRecordToHttpRecord).collect(Collectors.toList());

        return new ExistenciasEnTiendaFilterHttpResponse(httpFilter, httpRecords);
    }

}
