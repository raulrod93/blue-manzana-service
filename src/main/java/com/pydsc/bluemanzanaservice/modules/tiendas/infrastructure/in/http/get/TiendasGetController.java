package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Tiendas.PATH_API_TIENDAS_GET_PRODUCT_STOCK;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Tiendas.PATH_API_TIENDAS;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.ExistenciasEnTiendaFilter;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQuery;
import com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto.ExistenciasEnTiendaFilterHttpResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto.ExistenciasEnTiendaHttpRequest;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.SuccessResponse;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(PATH_API_TIENDAS)
@RequiredArgsConstructor
public class TiendasGetController {

    private final ExistenciasEnTiendaFilter tiendaFilter;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = PATH_API_TIENDAS_GET_PRODUCT_STOCK, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse<ExistenciasEnTiendaFilterHttpResponse>> filter(
            final ExistenciasEnTiendaHttpRequest request) {
    		final ExistenciasEnTiendaFilterQueryResponse response = tiendaFilter.filter(
                    ExistenciasEnTiendaFilterQuery.builder()
                    		.productoId(request.getProductoId())
                            .build());

            return ResponseEntity.ok()
                    .body(new SuccessResponse<>(ExistenciasEnTiendaHttpMapper.queryResponseToHttpResponse(response)));
    }

}
