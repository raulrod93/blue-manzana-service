package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto;

import lombok.Data;

@Data
public class ExistenciasEnTiendaHttpRequest {

    private String productoId;

}
