package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaCriteria;

public class CustomExistenciasEnTiendaRepositoryQueryBuilder {

    private CustomExistenciasEnTiendaRepositoryQueryBuilder() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    private static final String Q_BASE =
    	"SELECT" + 
    		" ex.cantidad as cantidad, " +
    		" pr.id as productoId, pr.nombre as productoNombre, pr.precio as productoPrecio, pr.fechadecreacion as productoFechaDeCreacion, pr.categoria as productoCategoria," + 
	    	" ti.id as tiendaId, ti.telefono as tiendaTelefono," + 
    		" di.calle as tiendaCalle, di.numero as tiendaNumero, di.otros as tiendaOtros, di.codigopostal as tiendaCodigopostal, di.localidad as tiendaLocalidad, di.provincia as tiendaProvincia" + 
	    	" FROM existenciasentiendas ex";

    private static final String Q_JOIN_PRODUCTOS = " INNER JOIN productos as pr ON ex.producto = pr.id";
    private static final String Q_JOIN_TIENDAS = " INNER JOIN tiendas as ti ON ex.entienda = ti.id";
    private static final String Q_JOIN_DIRECCIONES = " INNER JOIN direcciones as di ON ti.direccion = di.id";
    private static final String Q_WHERE_PRODUCTO_ID = " WHERE ex.producto = :productoId";

    private static final String P_PRODUCTO_ID = "productoId";

    static Query buildQuery(final ExistenciasEnTiendaCriteria criteria, final EntityManager entityManager) {

        final var query = entityManager.createNativeQuery(createQuery(criteria), Tuple.class);
        setParams(query, criteria);

        return query;
    }

    private static String createQuery(final ExistenciasEnTiendaCriteria criteria) {

        final StringBuilder query;

        query = new StringBuilder(Q_BASE);
        if (criteria.getProductoId() != null) {
        	query.append(Q_JOIN_PRODUCTOS);
        	query.append(Q_JOIN_TIENDAS);
        	query.append(Q_JOIN_DIRECCIONES);
        	query.append(Q_WHERE_PRODUCTO_ID);
        }

        return query.toString();
    }

    private static void setParams(final Query query, final ExistenciasEnTiendaCriteria criteria) {

        if (criteria.getProductoId() != null) {
        	query.setParameter(P_PRODUCTO_ID, Short.valueOf(criteria.getProductoId()));
        }
    }
}
