package com.pydsc.bluemanzanaservice.modules.tiendas.application.filter;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.ExistenciasEnTiendaFilter;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto.ExistenciasEnTiendaFilterQuery;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExistenciasEnTiendaFilter {
    private final ExistenciasEnTiendaRepository existenciasEnTiendaRepository;

    public ExistenciasEnTiendaFilterQueryResponse filter(final ExistenciasEnTiendaFilterQuery query) {
		final var criteria = ExistenciasEnTiendaFilterMapper.queryToExistenciasEnTiendaCriteria(query);
        final var product = existenciasEnTiendaRepository.getStock(criteria);
        return ExistenciasEnTiendaFilterMapper.productRecordToQueryResponse(product);

    }

}
