package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import java.io.Serializable;

public class ExistenciasEnTiendaEntityCompositeKey implements Serializable {
    private Short productoId;
    private Short tiendaId;
}