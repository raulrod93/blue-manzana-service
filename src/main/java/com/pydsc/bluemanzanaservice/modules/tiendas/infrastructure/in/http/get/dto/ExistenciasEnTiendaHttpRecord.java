package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.TiendaRecord;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExistenciasEnTiendaHttpRecord {

	
	private Short cantidad;
	
	private TiendaRecord tienda;
	
	private ProductoRecord producto;
	
}
