package com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.TiendaRecord;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ExistenciasEnTiendaFilterRecord {
	
	private Short cantidad;
	
	private TiendaRecord tienda;
	
	private ProductoRecord producto;

}
