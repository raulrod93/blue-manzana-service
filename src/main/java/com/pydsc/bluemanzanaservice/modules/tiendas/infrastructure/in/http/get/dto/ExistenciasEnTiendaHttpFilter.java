package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.in.http.get.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExistenciasEnTiendaHttpFilter {
	
	private String productoId;

}
