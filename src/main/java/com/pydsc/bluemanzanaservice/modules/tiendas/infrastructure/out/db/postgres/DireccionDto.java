package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class DireccionDto {

	private final Short id;
	
	private final String calle;
	
	private final Short numero;
	
	private final String otros;
	
	private final Short codigopostal;
	
	private final String localidad;
	
	private final String provincia;
}
