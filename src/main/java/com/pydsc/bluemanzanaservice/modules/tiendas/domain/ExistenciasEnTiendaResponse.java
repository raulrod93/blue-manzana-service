package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;

import lombok.Data;

@Data
public class ExistenciasEnTiendaResponse {
	
	private Short cantidad;
	
	private TiendaRecord tienda;
	
	private ProductoRecord producto;
	
}
