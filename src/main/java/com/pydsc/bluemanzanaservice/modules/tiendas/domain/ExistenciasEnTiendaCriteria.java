package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

public final class ExistenciasEnTiendaCriteria {
	
    private final String productoId;
	
    
    private ExistenciasEnTiendaCriteria(final Builder builder) {
    	
    	productoId = builder.productoId;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getProductoId() {
        return productoId;
    }
    
    public static final class Builder {
    	
        private String productoId;

        private Builder() {
            // Builder
        }

        public Builder productoId(String val) {
        	productoId = val;
            return this;
        }

        public ExistenciasEnTiendaCriteria build() {
            return new ExistenciasEnTiendaCriteria(this);
        }

    }

}
