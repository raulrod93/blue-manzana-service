package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

import com.pydsc.bluemanzanaservice.modules.shared.domain.DireccionRecord;

public class TiendaRecord {
	
	private final Short id;
	
	private final Short telefono;
	
	private final DireccionRecord direccion;
	
	private final byte[] imagen;
	

    public static TiendaRecord.Builder newBuilder() {
        return new TiendaRecord.Builder();
    }

    private TiendaRecord(final Builder builder) {

        this.id = builder.id;
        this.telefono = builder.telefono;
        this.direccion = builder.direccion;
        this.imagen = builder.imagen;
    }

    public static final class Builder {
    	
    	private Short id;
    	private Short telefono;
    	private DireccionRecord direccion;
    	private byte[] imagen;

        private Builder() {
            // Builder
        }

        public static Builder aTemperatureReportItem() {
            return new Builder();
        }
        public Builder id(Short id) {
            this.id = id;
            return this;
        }

        public Builder telefono(Short telefono) {
            this.telefono = telefono;
            return this;
        }

        public Builder direccion(DireccionRecord direccion) {
            this.direccion = direccion;
            return this;
        }

        public Builder imagen(byte[] imagen) {
            this.imagen = imagen;
            return this;
        }

        public TiendaRecord build() {
            return new TiendaRecord(this);
        }
    }

    public Short getId() {
        return id;
    }

    public Short gettelefono() {
        return telefono;
    }

    public DireccionRecord getdireccion() {
        return direccion;
    }

    public byte[] getimagen() {
        return imagen;
    }

    @Override
    public String toString() {
        return "TiendaRecord{" +
                "id=" + id +
                ", telefono=" + telefono +
                ", direccion=" + direccion +
                ", imagen=" + imagen +
                '}';
    }
}
