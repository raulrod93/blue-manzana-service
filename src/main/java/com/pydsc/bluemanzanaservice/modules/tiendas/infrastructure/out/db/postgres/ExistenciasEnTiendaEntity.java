package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.EntityConstants;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@IdClass(ExistenciasEnTiendaEntityCompositeKey.class)
@Table(name = EntityConstants.Tables.EXISTENCIAS_EN_TIENDA)
public class ExistenciasEnTiendaEntity {

    @Column(name = "cantidad", nullable = false)
    private Short cantidad;

    @Id
    @Column(name = "tiendaId", nullable = false)
    private Short tiendaId;

    @Column(name = "tiendaTelefono", nullable = false)
    private Short tiendaTelefono;

    @Column(name = "tiendaCalle", nullable = false)
    private String tiendaCalle;

    @Column(name = "tiendaNumero", nullable = false)
    private Short tiendaNumero;

    @Column(name = "tiendaOtros", nullable = false)
    private String tiendaOtros;

    @Column(name = "tiendaCodigopostal", nullable = false)
    private Short tiendaCodigopostal;

    @Column(name = "tiendaLocalidad", nullable = false)
    private String tiendaLocalidad;

    @Column(name = "tiendaProvincia", nullable = false)
    private String tiendaProvincia;

    @Id
    @Column(name = "productoId", nullable = false)
    private Short productoId;

    @Column(name = "productoNombre", nullable = false)
    private String productoNombre;

    @Column(name = "productoPrecio", nullable = false)
    private Float productoPrecio;

    @Override
    public String toString() {
        return "ProductoEntity{" +
                "cantidad=" + cantidad +
                // TODO
                '}';
    }
}
