package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

public interface ExistenciasEnTiendaRepository {

	ExistenciasEnTiendaCriteriaResponse getStock(ExistenciasEnTiendaCriteria criteria);

}
