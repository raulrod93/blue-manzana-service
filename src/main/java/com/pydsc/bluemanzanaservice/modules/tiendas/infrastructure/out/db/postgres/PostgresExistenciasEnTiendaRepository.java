package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import static com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres.CustomExistenciasEnTiendaRepositoryQueryBuilder.buildQuery;

import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaCriteria;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaRepository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PostgresExistenciasEnTiendaRepository implements ExistenciasEnTiendaRepository {

    private final PostgresJpaExistenciasEnTiendaRepository repository;
    private final EntityManager entityManager;

    public PostgresExistenciasEnTiendaRepository(final PostgresJpaExistenciasEnTiendaRepository repository,
    		@Qualifier("blueManzanaEntityManager") final EntityManager entityManager) {
    	this.repository = repository;
        this.entityManager = entityManager;
    }

    @Override
    public ExistenciasEnTiendaCriteriaResponse getStock(final ExistenciasEnTiendaCriteria criteria) {

        List<Tuple> rows = buildQuery(criteria, entityManager).getResultList();
        List<ExistenciasEnTiendaDto> dtos = rows.isEmpty()
                ? Collections.emptyList() : ExistenciasEnTiendaEntityMapper.convertResultListToExistenciasEnTiendaDtoList(rows);

        final List<ExistenciasEnTiendaRecord> records = dtos.stream()
                .map(ExistenciasEnTiendaEntityMapper::productoDtoToExistenciasEnTiendaRecord)
                .collect(Collectors.toList());

        return new ExistenciasEnTiendaCriteriaResponse(criteria, records);
    }
}
