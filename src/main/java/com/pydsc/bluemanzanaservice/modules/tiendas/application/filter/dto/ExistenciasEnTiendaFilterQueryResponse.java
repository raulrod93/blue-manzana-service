package com.pydsc.bluemanzanaservice.modules.tiendas.application.filter.dto;

import java.util.List;


import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ExistenciasEnTiendaFilterQueryResponse {

    private final ExistenciasEnTiendaFilterQuery filter;

    private final List<ExistenciasEnTiendaFilterRecord> records;

}
