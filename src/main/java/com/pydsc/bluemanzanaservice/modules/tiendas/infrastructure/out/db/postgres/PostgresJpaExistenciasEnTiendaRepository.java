package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresJpaExistenciasEnTiendaRepository extends JpaRepository<ExistenciasEnTiendaEntity, Short> {
    // PostgresJpaProductoRepository

}
