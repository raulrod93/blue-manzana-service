package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

import java.util.List;

public class ExistenciasEnTiendaCriteriaResponse {

    private ExistenciasEnTiendaCriteria filter;

    private final List<ExistenciasEnTiendaRecord> records;

    public ExistenciasEnTiendaCriteriaResponse(final ExistenciasEnTiendaCriteria filter, final List<ExistenciasEnTiendaRecord> records) {

        this.filter = filter;
        this.records = records;
    }

    public ExistenciasEnTiendaCriteria getFilter() {
        return filter;
    }


    public List<ExistenciasEnTiendaRecord> getRecords() {
        return records;
    }

    @Override
    public String toString() {
        return "ProductoCriteriaResponse{" +
                "filter=" + filter +
                ", records=" + records +
                '}';
    }

}
