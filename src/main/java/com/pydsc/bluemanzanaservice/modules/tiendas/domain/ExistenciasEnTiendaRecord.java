package com.pydsc.bluemanzanaservice.modules.tiendas.domain;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;

public class ExistenciasEnTiendaRecord {
	
	private final Short cantidad;
	
	private final ProductoRecord producto;
	
	private final TiendaRecord tienda;
	
    public static ExistenciasEnTiendaRecord.Builder newBuilder() {
        return new ExistenciasEnTiendaRecord.Builder();
    }

    private ExistenciasEnTiendaRecord(final Builder builder) {

        this.cantidad = builder.cantidad;
        this.producto = builder.producto;
        this.tienda = builder.tienda;
    }

    public static final class Builder {
    	
    	private Short cantidad;
    	
    	private ProductoRecord producto;
    	
    	private TiendaRecord tienda;

        private Builder() {
            // Builder
        }

        public static Builder aTemperatureReportItem() {
            return new Builder();
        }
        public Builder cantidad(Short cantidad) {
            this.cantidad = cantidad;
            return this;
        }

        public Builder producto(ProductoRecord producto) {
            this.producto = producto;
            return this;
        }

        public Builder tienda(TiendaRecord tienda) {
            this.tienda = tienda;
            return this;
        }

        public ExistenciasEnTiendaRecord build() {
            return new ExistenciasEnTiendaRecord(this);
        }

    }

    public Short getCantidad() {
        return cantidad;
    }

    public ProductoRecord getProducto() {
        return producto;
    }

    public TiendaRecord getTienda() {
        return tienda;
    }

    @Override
    public String toString() {
        return "ProductoRecord{" +
                "cantidad=" + cantidad +
                ", producto=" + producto +
                ", tienda=" + tienda +
                '}';
    }
}
