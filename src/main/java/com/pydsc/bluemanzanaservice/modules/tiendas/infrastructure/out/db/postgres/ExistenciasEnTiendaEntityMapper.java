package com.pydsc.bluemanzanaservice.modules.tiendas.infrastructure.out.db.postgres;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres.ProductoDto;
import com.pydsc.bluemanzanaservice.modules.shared.domain.DireccionRecord;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.ExistenciasEnTiendaRecord;
import com.pydsc.bluemanzanaservice.modules.tiendas.domain.TiendaRecord;

public final class ExistenciasEnTiendaEntityMapper {

    private ExistenciasEnTiendaEntityMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static List<ExistenciasEnTiendaDto> convertResultListToExistenciasEnTiendaDtoList(final List<Tuple> tuples) {

        return tuples.stream()
                .map(ExistenciasEnTiendaEntityMapper::convertRowToExistenciasEnTiendaDto)
                .collect(Collectors.toList());
    }

    private static ExistenciasEnTiendaDto convertRowToExistenciasEnTiendaDto(final Tuple tuple) {
    	ProductoDto producto = ProductoDto.builder()
        		.id(tuple.get("productoId", Short.class))
        		.categoria(tuple.get("productoCategoria", Short.class))
        		.nombre(tuple.get("productoNombre", String.class))
        		.precio(tuple.get("productoPrecio", Float.class))
        		.fechaDeCreacion(tuple.get("productoFechaDeCreacion", Date.class))
                .build();
    	
    	DireccionDto direccionTienda = DireccionDto.builder()
    			.calle(tuple.get("tiendaCalle", String.class))
    			.numero(tuple.get("tiendaNumero", Short.class))
    			.codigopostal(tuple.get("tiendaCodigopostal", Short.class))
    			.otros(tuple.get("tiendaOtros", String.class))
    			.provincia(tuple.get("tiendaProvincia", String.class))
    			.localidad(tuple.get("tiendaLocalidad", String.class))
    			.build();
    	
    	TiendaDto tienda = TiendaDto.builder()
        		.id(tuple.get("tiendaId", Short.class))
        		.telefono(tuple.get("tiendaTelefono", Short.class))
        		.direccion(direccionTienda)
                .build();

        return ExistenciasEnTiendaDto.builder()
        		.cantidad(tuple.get("cantidad", Short.class))
        		.tienda(tienda)
        		.producto(producto)
                .build();
    }

    static ExistenciasEnTiendaRecord productoDtoToExistenciasEnTiendaRecord(final ExistenciasEnTiendaDto input) {
    	ProductoDto productoDto = input.getProducto();
    	TiendaDto tiendaDto = input.getTienda();
    	DireccionDto direccionTiendaDto = tiendaDto.getDireccion();

    	ProductoRecord producto = ProductoRecord.newBuilder()
        		.id(productoDto.getId())
        		.categoria(productoDto.getCategoria())
        		.nombre(productoDto.getNombre())
        		.precio(productoDto.getPrecio())
        		.fechaDeCreacion(productoDto.getFechaDeCreacion())
                .build();
    	
    	DireccionRecord direccionTienda = DireccionRecord.newBuilder()
    			.calle(direccionTiendaDto.getCalle())
    			.numero(direccionTiendaDto.getNumero())
    			.codigopostal(direccionTiendaDto.getCodigopostal())
    			.otros(direccionTiendaDto.getOtros())
    			.localidad(direccionTiendaDto.getLocalidad())
    			.provincia(direccionTiendaDto.getProvincia())
    			.build();

    	TiendaRecord tienda = TiendaRecord.newBuilder()
        		.id(tiendaDto.getId())
        		.telefono(tiendaDto.getTelefono())
        		.direccion(direccionTienda)
                .build();
    	
        return ExistenciasEnTiendaRecord.newBuilder()
                .cantidad(input.getCantidad())
                .tienda(tienda)
                .producto(producto)
                .build();
    }

    static ExistenciasEnTiendaRecord productoEntityToExistenciasEnTiendaRecord(final ExistenciasEnTiendaEntity input) {
    	ProductoRecord producto = ProductoRecord.newBuilder()
        		.id(input.getProductoId())
        		.nombre(input.getProductoNombre())
        		.precio(input.getProductoPrecio())
                .build();
    	
    	DireccionRecord direccionTienda = DireccionRecord.newBuilder()
    			.calle(input.getTiendaCalle())
    			.numero(input.getTiendaNumero())
    			.codigopostal(input.getTiendaCodigopostal())
    			.otros(input.getTiendaOtros())
    			.localidad(input.getTiendaLocalidad())
    			.provincia(input.getTiendaProvincia())
    			.build();

    	TiendaRecord tienda = TiendaRecord.newBuilder()
        		.id(input.getTiendaId())
        		.telefono(input.getTiendaTelefono())
        		.direccion(direccionTienda)
                .build();
    	
        return ExistenciasEnTiendaRecord.newBuilder()
                .cantidad(input.getCantidad())
                .tienda(tienda)
                .producto(producto)
                .build();
    }
}
