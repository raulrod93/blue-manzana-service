package com.pydsc.bluemanzanaservice.modules.pedidos.application.updater.dto;

import java.util.Date;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoIdRecord;

import lombok.Data;

@Data
public class PedidoUpdateCommand {
	
    private final PedidoIdRecord id;

    private final Short cantidad;

    private final Date fecha;

    private final Short estado;

    private final Short producto;

    private final Short solicitadoen;

    private final Short preparacion;
}
