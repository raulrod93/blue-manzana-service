package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PedidoFinderHttpResponse {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final PedidoHttpRecord record;

}
