package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.RegistroDeEntregaCreator;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.dto.RegistroDeEntregaCreatorCommand;
import com.pydsc.bluemanzanaservice.modules.shared.domain.EstadoPedido;

@Service
public class PedidoDomainUpdater {

    private final PedidoRepository pedidoRepository;
    
    private final RegistroDeEntregaCreator registroDeEntregaCreator;

    public PedidoDomainUpdater(PedidoRepository pedidoRepository, RegistroDeEntregaCreator registroDeEntregaCreator) {
        this.pedidoRepository = pedidoRepository;
        this.registroDeEntregaCreator = registroDeEntregaCreator;
    }

    public void update(final PedidoRecord pedido) {
    	Optional<PedidoRecord> record = pedidoRepository.findById(pedido.getId());

        if (record.isPresent()) {
        	PedidoRecord oldPedido = record.get();
            pedidoRepository.save(pedido);
            
            if (oldPedido.getEstado() != Short.valueOf(EstadoPedido.DELIVERED.getValue()) && pedido.getEstado() == Short.valueOf(EstadoPedido.DELIVERED.getValue())) {
            	RegistroDeEntregaCreatorCommand registroDeEntregaCreatorCommand = new RegistroDeEntregaCreatorCommand(new Date(), new Date(), "11111111H");
            	registroDeEntregaCreator.create(registroDeEntregaCreatorCommand);
            }
        } else {
            throw PedidoNotFoundException.ofId(pedido.getId().getId().toString());
        }
    }
}
