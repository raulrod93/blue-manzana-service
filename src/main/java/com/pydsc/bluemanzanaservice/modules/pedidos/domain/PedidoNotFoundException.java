package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public class PedidoNotFoundException extends RuntimeException {

    public PedidoNotFoundException(String message) {
        super(message);
    }

    public static PedidoNotFoundException ofId(final String id) {
        return new PedidoNotFoundException("Pedido con [id] " + id + " no encontrado");
    }
}
