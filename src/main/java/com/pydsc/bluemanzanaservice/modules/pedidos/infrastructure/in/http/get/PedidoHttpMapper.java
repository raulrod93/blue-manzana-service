package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get;

import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto.PedidoHttpFinder;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto.PedidoHttpRecord;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQuery;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQueryResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto.PedidoFinderHttpResponse;

final class PedidoHttpMapper {

    private PedidoHttpMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static PedidoHttpFinder queryFinderToTHttpFinder(final PedidoFinderQuery finder) {

        return PedidoHttpFinder.builder()
                .id(finder.getId())
                .build();
    }

    static PedidoHttpRecord queryRecordToHttpRecord(final PedidoFinderRecord reportFinderRecord) {

        return PedidoHttpRecord.builder()
                .id(reportFinderRecord.getId())
                .cantidad(reportFinderRecord.getCantidad())
                .fecha(reportFinderRecord.getFecha())
                .estado(reportFinderRecord.getEstado())
                .producto(reportFinderRecord.getProducto())
                .solicitadoen(reportFinderRecord.getSolicitadoen())
                .preparacion(reportFinderRecord.getPreparacion())
                .build();
    }

    static PedidoFinderHttpResponse queryResponseToHttpResponse(
            final PedidoFinderQueryResponse response) {

        final var httpRecord = queryRecordToHttpRecord(response.getPedido());

        return new PedidoFinderHttpResponse(httpRecord);
    }
}
