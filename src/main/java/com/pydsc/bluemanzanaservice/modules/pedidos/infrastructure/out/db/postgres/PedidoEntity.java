package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.EntityConstants;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = EntityConstants.Tables.PEDIDOS)
public class PedidoEntity {

    @Id
    @Column(name = "id")
    private Short id;

    @Column(name = "cantidad", nullable = false)
    private Short cantidad;

    @Column(name = "fecha", nullable = false)
    private Date fecha;

    @Column(name = "estado", nullable = false)
    private Short estado;

    @Column(name = "producto", nullable = false)
    private Short producto;

    @Column(name = "solicitadoen", nullable = false)
    private Short solicitadoen;

    @Column(name = "preparacion", nullable = false)
    private Short preparacion;

    @Override
    public String toString() {
        return "PedidoEntity{";
        // TODO
    }
}
