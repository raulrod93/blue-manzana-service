package com.pydsc.bluemanzanaservice.modules.pedidos.application.creator;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.dto.RegistroDeEntregaCreatorCommand;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaDomainFinder;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaIdRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaRepository;

@Service
public class RegistroDeEntregaCreator {

    private final RegistroDeEntregaRepository registroDeEntregaRepository;
    final RegistroDeEntregaDomainFinder registroDeEntregaDomainFinder;

    public RegistroDeEntregaCreator(final RegistroDeEntregaRepository registroDeEntregaRepository,
            final RegistroDeEntregaDomainFinder registroDeEntregaDomainFinder) {
        this.registroDeEntregaRepository = registroDeEntregaRepository;
        this.registroDeEntregaDomainFinder = registroDeEntregaDomainFinder;
    }

    public Short create(final RegistroDeEntregaCreatorCommand command) {
        final var registroDeEntrega = generateRegistroDeEntrega(command);
        registroDeEntregaRepository.save(registroDeEntrega);
        return registroDeEntrega.getId().getId();
    }

    private RegistroDeEntregaRecord generateRegistroDeEntrega(final RegistroDeEntregaCreatorCommand command) {
        final Short numerator = registroDeEntregaDomainFinder.findNextNumerator();
        final RegistroDeEntregaIdRecord registroDeEntregaId = new RegistroDeEntregaIdRecord(String.format("%04d", numerator));

        return RegistroDeEntregaRecord.newBuilder()
        		.id(registroDeEntregaId)
        		.fecha(command.getFecha())
        		.hora(command.getHora())
        		.dniquienfirma(command.getDniquienfirma()).build();
    }
}
