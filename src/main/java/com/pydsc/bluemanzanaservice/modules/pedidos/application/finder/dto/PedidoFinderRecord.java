package com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class PedidoFinderRecord {
	
    private Short id;

    private Short cantidad;

    private Date fecha;

    private Short estado;

    private Short producto;

    private Short solicitadoen;

    private Short preparacion;

}
