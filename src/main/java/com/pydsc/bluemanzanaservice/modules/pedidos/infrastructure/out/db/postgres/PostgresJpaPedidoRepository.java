package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresJpaPedidoRepository extends JpaRepository<PedidoEntity, Short> {
    // PostgresJpaProductoRepository

}
