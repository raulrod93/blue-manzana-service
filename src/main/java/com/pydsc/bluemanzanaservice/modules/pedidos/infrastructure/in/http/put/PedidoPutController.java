package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.put;

import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS_CONFIRMAR_ENTREGA;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.updater.PedidoUpdater;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.updater.dto.PedidoUpdateCommand;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoIdRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.put.dto.PedidoHttpRequest;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiResponseFactory;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.SuccessResponse;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(PATH_API_PEDIDOS)
@RequiredArgsConstructor
public class PedidoPutController {

    private final PedidoUpdater pedidoUpdater;

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = PATH_API_PEDIDOS_CONFIRMAR_ENTREGA, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse<String>> confirmDelivery(@RequestBody final PedidoHttpRequest request) {

    	pedidoUpdater.update(
    			new PedidoUpdateCommand(
    					new PedidoIdRecord(request.getId().toString()),
    					request.getCantidad(),
    					request.getFecha(),
    					request.getEstado(),
    					request.getProducto(),
    					request.getSolicitadoen(),
    					request.getPreparacion()
    			)
    	);

        return ResponseEntity.status(HttpStatus.OK)
                .body(ApiResponseFactory.basicStringSuccessResponse("Order updated successfully"));
    }
}
