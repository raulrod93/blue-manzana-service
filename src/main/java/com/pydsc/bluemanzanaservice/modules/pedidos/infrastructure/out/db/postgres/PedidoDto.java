package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class PedidoDto {
	
	private final Short id;
	
	private final Short cantidad;
	
	private final Date fecha;
	
	private final Short estado;
	
	private final Short producto;
	
	private final Short solicitadoen;
	
	private final Short preparacion;
}
