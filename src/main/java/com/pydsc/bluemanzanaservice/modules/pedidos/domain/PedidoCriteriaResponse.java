package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public class PedidoCriteriaResponse {

    private final PedidoRecord record;

    public PedidoCriteriaResponse(final PedidoRecord record) {

        this.record = record;
    }

    public PedidoRecord getRecord() {
        return record;
    }

    @Override
    public String toString() {
        return "PedidoCriteriaResponse{" +
// TODO
                '}';
    }

}
