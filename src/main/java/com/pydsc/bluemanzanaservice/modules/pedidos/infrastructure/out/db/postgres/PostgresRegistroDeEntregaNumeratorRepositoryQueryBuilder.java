package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;

public final class PostgresRegistroDeEntregaNumeratorRepositoryQueryBuilder {

    private PostgresRegistroDeEntregaNumeratorRepositoryQueryBuilder() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static Query buildQuery(final EntityManager entityManager) {

        return entityManager.createNativeQuery(new StringBuilder("SELECT distinct id FROM registrodeentregas ORDER BY id desc").toString(), Tuple.class);
    }
}
