package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.post.dto;

import java.sql.Time;
import java.util.Date;

import lombok.Data;

@Data
public class RegistroDeEntregaPostHttpRequest {

    private Short id;
    private Date fecha;
    private Time hora;
    private String dniquienfirma;

}
