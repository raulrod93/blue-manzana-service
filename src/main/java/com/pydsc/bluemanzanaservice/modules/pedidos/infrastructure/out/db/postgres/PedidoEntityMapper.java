package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoIdRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRecord;

public final class PedidoEntityMapper {

    private PedidoEntityMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static List<PedidoDto> convertResultListToPedidoDtoList(final List<Tuple> tuples) {

        return tuples.stream()
                .map(PedidoEntityMapper::convertRowToPedidoDto)
                .collect(Collectors.toList());
    }

    private static PedidoDto convertRowToPedidoDto(final Tuple tuple) {

        return PedidoDto.builder()
        		.id(tuple.get("id", Short.class))
        		.cantidad(tuple.get("cantidad", Short.class))
        		.fecha(tuple.get("fecha", Date.class))
        		.estado(tuple.get("estado", Short.class))
        		.producto(tuple.get("producto", Short.class))
        		.solicitadoen(tuple.get("solicitadoen", Short.class))
        		.preparacion(tuple.get("preparacion", Short.class))
                .build();
    }

    static PedidoRecord pedidoDtoToPedidoRecord(final PedidoDto input) {

        return PedidoRecord.newBuilder()
                .id(new PedidoIdRecord(input.getId().toString()))
                .cantidad(input.getCantidad())
                .fecha(input.getFecha())
                .estado(input.getEstado())
                .producto(input.getProducto())
                .solicitadoen(input.getSolicitadoen())
                .preparacion(input.getPreparacion())
                .build();
    }

    static PedidoRecord pedidoEntityToPedidoRecord(final PedidoEntity input) {

        return PedidoRecord.newBuilder()
                .id(new PedidoIdRecord(input.getId().toString()))
                .cantidad(input.getCantidad())
                .fecha(input.getFecha())
                .estado(input.getEstado())
                .producto(input.getProducto())
                .solicitadoen(input.getSolicitadoen())
                .preparacion(input.getPreparacion())
                .build();
    }
    
    static PedidoEntity pedidoRecordToPedidoEntity(final PedidoRecord input) {

        final var output = new PedidoEntity();
        output.setId(input.getId().getId());
        output.setCantidad(input.getCantidad());
        output.setFecha(input.getFecha());
        output.setEstado(input.getEstado());
        output.setProducto(input.getProducto());
        output.setSolicitadoen(input.getSolicitadoen());
        output.setPreparacion(input.getPreparacion());

        return output;
    }
}
