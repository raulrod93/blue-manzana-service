package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import java.util.Date;

import lombok.Data;

@Data
public class PedidoResponse {
	
	private Short id;
	
	private Short cantidad;
	
	private Date fecha;
	
	private Short estado;
	
	private Short producto;
	
	private Short solicitadoen;
	
	private Short preparacion;

}
