package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaRecord;

public final class RegistroDeEntregaEntityMapper {

    private RegistroDeEntregaEntityMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }
    
    static RegistroDeEntregaEntity registroDeEntregaRecordToRegistroDeEntregaEntity(final RegistroDeEntregaRecord input) {

        final var output = new RegistroDeEntregaEntity();
        output.setId(input.getId().getId());
        output.setFecha(input.getFecha());
        output.setHora(input.getHora());
        output.setDniquienfirma(input.getDniquienfirma());

        return output;
    }
}
