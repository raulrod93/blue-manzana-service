package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public class PedidoIdRecord {

    private final Short id;

    public PedidoIdRecord(final String id) {
        this.id = Short.valueOf(id);
    }

    public Short getId() {
        return id;
    }
}
