package com.pydsc.bluemanzanaservice.modules.pedidos.application.finder;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQuery;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQueryResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoCriteria;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRecord;

final class PedidoFinderMapper {

    private PedidoFinderMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }


    public static PedidoCriteria queryToPedidoCriteria(final PedidoFinderQuery query) {
        return PedidoCriteria.newBuilder()
        		.id(query.getId())
                .build();
    }

    static PedidoFinderQueryResponse pedidoRecordToQueryResponse(
            final PedidoCriteriaResponse pedidoCriteriaResponse) {

        final PedidoFinderRecord pedido = pedidoRecordToResponse(pedidoCriteriaResponse.getRecord());

        return new PedidoFinderQueryResponse(pedido);
    }

    static PedidoFinderRecord pedidoRecordToResponse(final PedidoRecord pedidoRecord) {

        return baseBuilder(pedidoRecord).build();
    }


    static PedidoFinderQuery criteriaToQuery(final PedidoCriteria criteria) {
        return PedidoFinderQuery.builder()
        		.id(criteria.getId())
                .build();
    }

    private static PedidoFinderRecord.Builder baseBuilder(final PedidoRecord pedidoRecord) {

        return PedidoFinderRecord.builder()
        		.id(pedidoRecord.getId().getId())
        		.cantidad(pedidoRecord.getCantidad())
        		.fecha(pedidoRecord.getFecha())
        		.estado(pedidoRecord.getEstado())
        		.producto(pedidoRecord.getProducto())
        		.solicitadoen(pedidoRecord.getSolicitadoen())
        		.preparacion(pedidoRecord.getPreparacion());

    }
}
