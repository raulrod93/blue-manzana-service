package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public interface RegistroDeEntregaRepository {

    void save(RegistroDeEntregaRecord registro);

    Short findNextNumerator();
}
