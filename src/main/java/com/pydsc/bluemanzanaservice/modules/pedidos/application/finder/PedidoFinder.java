package com.pydsc.bluemanzanaservice.modules.pedidos.application.finder;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQuery;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQueryResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoNotFoundException;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PedidoFinder {
    private final PedidoRepository pedidoRepository;

    public PedidoFinderQueryResponse finder(final PedidoFinderQuery query) {
        final var criteria = PedidoFinderMapper.queryToPedidoCriteria(query);
        final var pedido = pedidoRepository.finder(criteria);
        PedidoFinderQueryResponse queryResponse;
        
        try {
            queryResponse = PedidoFinderMapper.pedidoRecordToQueryResponse(pedido);
			
		} catch (Exception e) {
        	throw PedidoNotFoundException.ofId(query.getId());
		}
        
        return queryResponse;

    }
}
