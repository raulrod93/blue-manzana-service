package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.put.dto;

import java.util.Date;

import lombok.Data;

@Data
public class PedidoHttpRequest {

	private String id;

    private Short cantidad;

    private Date fecha;

    private Short estado;

    private Short producto;

    private Short solicitadoen;

    private Short preparacion;

}
