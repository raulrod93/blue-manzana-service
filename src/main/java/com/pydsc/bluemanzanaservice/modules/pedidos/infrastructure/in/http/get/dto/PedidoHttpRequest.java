package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto;

import lombok.Data;

@Data
public class PedidoHttpRequest {
	
	private String id;

}
