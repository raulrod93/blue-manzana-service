package com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.dto;

import java.sql.Time;
import java.util.Date;

import lombok.Data;

@Data
public class RegistroDeEntregaCreatorCommand {

    private final Date fecha;
    private final Date hora;
    private final String dniquienfirma;

}
