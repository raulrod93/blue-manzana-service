package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public class RegistroDeEntregaIdRecord {

    private final Short id;

    public RegistroDeEntregaIdRecord(final String id) {
        this.id = Short.valueOf(id);
    }

    public Short getId() {
        return id;
    }
}
