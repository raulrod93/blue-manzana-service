package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.post;

import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS_CREAR_REGISTRO_DE_ENTREGA;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.RegistroDeEntregaCreator;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.creator.dto.RegistroDeEntregaCreatorCommand;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.post.dto.RegistroDeEntregaPostHttpRequest;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiResponseFactory;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.SuccessResponse;

@RestController
@RequestMapping(PATH_API_PEDIDOS)
public class PedidoPostController {

    private final RegistroDeEntregaCreator registroDeEntregaCreator;

    public PedidoPostController(final RegistroDeEntregaCreator registroDeEntregaCreator) {
        this.registroDeEntregaCreator = registroDeEntregaCreator;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = PATH_API_PEDIDOS_CREAR_REGISTRO_DE_ENTREGA, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse<String>> save(
            @RequestBody final RegistroDeEntregaPostHttpRequest request) {

        Short registroDeEntregaId = registroDeEntregaCreator.create(new RegistroDeEntregaCreatorCommand(
        		request.getFecha(),
        		request.getHora(),
        		request.getDniquienfirma()
		));

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ApiResponseFactory.basicStringSuccessResponse("Registro de entrega creado correctamente"));
    }
}
