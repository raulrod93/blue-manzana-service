package com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PedidoFinderQueryResponse {

    private final PedidoFinderRecord pedido;

}
