package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.EntityConstants;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = EntityConstants.Tables.REGISTROS_DE_ENTREGA)
public class RegistroDeEntregaEntity {

    @Id
    @Column(name = "id")
    private Short id;

    @Column(name = "fecha", nullable = false)
    private Date fecha;

    @Column(name = "hora", nullable = false)
    private Date hora;

    @Column(name = "dniquienfirma", nullable = false)
    private String dniquienfirma;

    @Override
    public String toString() {
        return "RegistroDeEntregaEntity{";
        // TODO
    }
}
