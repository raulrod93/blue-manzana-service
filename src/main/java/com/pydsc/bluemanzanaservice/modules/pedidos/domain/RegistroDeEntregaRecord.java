package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import java.sql.Time;
import java.util.Date;

public class RegistroDeEntregaRecord {

    private final RegistroDeEntregaIdRecord id;
    
    private final Date fecha;
    
    private final Date hora;
    
    private final String dniquienfirma;
    

    public static RegistroDeEntregaRecord.Builder newBuilder() {
        return new RegistroDeEntregaRecord.Builder();
    }

    private RegistroDeEntregaRecord(final Builder builder) {

        this.id = builder.id;
        this.fecha = builder.fecha;
        this.hora = builder.hora;
        this.dniquienfirma = builder.dniquienfirma;
    }

	public static final class Builder {

        private RegistroDeEntregaIdRecord id;
        private Date fecha;
        private Date hora;
        private String dniquienfirma;

        private Builder() {
            // Builder
        }

        public static Builder aTemperatureReportItem() {
            return new Builder();
        }
        public Builder id(RegistroDeEntregaIdRecord id) {
            this.id = id;
            return this;
        }

        public Builder fecha(Date fecha) {
            this.fecha = fecha;
            return this;
        }

        public Builder hora(Date hora) {
            this.hora = hora;
            return this;
        }

        public Builder dniquienfirma(String dniquienfirma) {
            this.dniquienfirma = dniquienfirma;
            return this;
        }

        public RegistroDeEntregaRecord build() {
            return new RegistroDeEntregaRecord(this);
        }
    }

    public RegistroDeEntregaIdRecord getId() {
        return id;
    }

    public Date getFecha() {
        return fecha;
    }

    public Date getHora() {
        return hora;
    }

    public String getDniquienfirma() {
        return dniquienfirma;
    }

}
