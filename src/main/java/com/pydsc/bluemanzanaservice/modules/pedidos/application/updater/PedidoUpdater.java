package com.pydsc.bluemanzanaservice.modules.pedidos.application.updater;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.updater.dto.PedidoUpdateCommand;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoDomainUpdater;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRecord;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PedidoUpdater {

    final PedidoDomainUpdater pedidoDomainUpdater;

    public void update(final PedidoUpdateCommand command) {
    	pedidoDomainUpdater.update(
			PedidoRecord.newBuilder()
                .id(command.getId())
                .cantidad(command.getCantidad())
                .fecha(command.getFecha())
                .estado(command.getEstado())
                .producto(command.getProducto())
                .solicitadoen(command.getSolicitadoen())
                .preparacion(command.getPreparacion())
                .build()
    		);
    }
}
