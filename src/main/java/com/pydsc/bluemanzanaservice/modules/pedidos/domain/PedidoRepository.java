package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import java.util.Optional;

public interface PedidoRepository {

	PedidoCriteriaResponse finder(PedidoCriteria criteria);

    Optional<PedidoRecord> findById(PedidoIdRecord id);

    void save(PedidoRecord pedido);
}
