package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import static com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres.PostgresRegistroDeEntregaNumeratorRepositoryQueryBuilder.buildQuery;

import java.util.List;


@Repository
public class PostgresRegistroDeEntregaNumeratorRepository {

    private final EntityManager entityManager;

    public PostgresRegistroDeEntregaNumeratorRepository(@Qualifier("blueManzanaEntityManager") final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public Short findNextNumerator() {
    	Short firstFreeNumerator = 1;
        List<Tuple> ids = buildQuery(entityManager).getResultList();
        
        Short id = ids.get(0).get("id", Short.class);
        
        if (id != null) firstFreeNumerator = (short) (firstFreeNumerator + id);

        return firstFreeNumerator;
    }

}
