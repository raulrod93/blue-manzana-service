package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoCriteria;

public class CustomPedidoRepositoryQueryBuilder {

    private CustomPedidoRepositoryQueryBuilder() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    private static final String Q_BASE =
            "SELECT pe.id AS id," +
					" pe.cantidad AS cantidad," +
                    " pe.fecha AS fecha," +
                    " pe.estado AS estado," +
                    " pe.producto AS producto," +
                    " pe.solicitadoen AS solicitadoen," +
                    " pe.preparacion AS preparacion" +
                    " FROM pedidosdesuministrodeproducto pe";

    private static final String Q_FIND_ID = " WHERE pe.id = :id";

    private static final String P_ID = "id";

    static Query buildQuery(final PedidoCriteria criteria, final EntityManager entityManager) {

        final var query = entityManager.createNativeQuery(createQuery(criteria), Tuple.class);
        setParams(query, criteria);

        return query;
    }

    private static String createQuery(final PedidoCriteria criteria) {

        final StringBuilder query;

        query = new StringBuilder(Q_BASE);
        if (criteria.getId() != null) query.append(Q_FIND_ID);

        return query.toString();
    }

    private static void setParams(final Query query, final PedidoCriteria criteria) {

        if (criteria.getId() != null) query.setParameter(P_ID, Short.valueOf(criteria.getId()));
    }
}
