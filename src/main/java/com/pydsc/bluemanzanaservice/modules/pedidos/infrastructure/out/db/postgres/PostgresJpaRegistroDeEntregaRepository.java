package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresJpaRegistroDeEntregaRepository extends JpaRepository<RegistroDeEntregaEntity, Short> {

    // PostgresJpaRegistroDeEntregaRepository

}
