package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import org.springframework.stereotype.Service;

@Service
public class RegistroDeEntregaDomainFinder {

    private final RegistroDeEntregaRepository registroDeEntregaRepository;

    public RegistroDeEntregaDomainFinder(final RegistroDeEntregaRepository registroDeEntregaRepository) {
        this.registroDeEntregaRepository = registroDeEntregaRepository;
    }

    public Short findNextNumerator() {
        return registroDeEntregaRepository.findNextNumerator();
    }

}
