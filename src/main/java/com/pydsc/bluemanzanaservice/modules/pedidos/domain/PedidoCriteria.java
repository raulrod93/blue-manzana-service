package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

public final class PedidoCriteria {
	
    private final String id;
	
    private PedidoCriteria(final Builder builder) {

        id = builder.id;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public static final class Builder {

        private String id;

        private Builder() {
            // Builder
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public PedidoCriteria build() {
            return new PedidoCriteria(this);
        }

    }

}
