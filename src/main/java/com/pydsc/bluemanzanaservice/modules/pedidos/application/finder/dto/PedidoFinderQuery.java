package com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class PedidoFinderQuery {

    private String id;
}
