package com.pydsc.bluemanzanaservice.modules.pedidos.domain;

import java.util.Date;

public class PedidoRecord {
	
    private final PedidoIdRecord id;

    private final Short cantidad;

    private final Date fecha;

    private final Short estado;

    private final Short producto;

    private final Short solicitadoen;

    private final Short preparacion;

    public static PedidoRecord.Builder newBuilder() {
        return new PedidoRecord.Builder();
    }

    private PedidoRecord(final Builder builder) {

        this.id = builder.id;
        this.cantidad = builder.cantidad;
        this.fecha = builder.fecha;
        this.estado = builder.estado;
        this.producto = builder.producto;
        this.solicitadoen = builder.solicitadoen;
        this.preparacion = builder.preparacion;
    }

	public static final class Builder {

        private PedidoIdRecord id;
        private Short cantidad;
        private Date fecha;
        private Short estado;
        private Short producto;
        private Short solicitadoen;
        private Short preparacion;

        private Builder() {
            // Builder
        }

        public static Builder aTemperatureReportItem() {
            return new Builder();
        }
        public Builder id(PedidoIdRecord id) {
            this.id = id;
            return this;
        }

        public Builder cantidad(Short cantidad) {
            this.cantidad = cantidad;
            return this;
        }

        public Builder fecha(Date fecha) {
            this.fecha = fecha;
            return this;
        }

        public Builder estado(Short estado) {
            this.estado = estado;
            return this;
        }

        public Builder producto(Short producto) {
            this.producto = producto;
            return this;
        }

        public Builder solicitadoen(Short solicitadoen) {
            this.solicitadoen = solicitadoen;
            return this;
        }

        public Builder preparacion(Short preparacion) {
            this.preparacion = preparacion;
            return this;
        }

        public PedidoRecord build() {
            return new PedidoRecord(this);
        }
    }

    public PedidoIdRecord getId() {
        return id;
    }

    public Short getCantidad() {
        return cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public Short getEstado() {
        return estado;
    }

    public Short getProducto() {
        return producto;
    }

    public Short getSolicitadoen() {
        return solicitadoen;
    }

    public Short getPreparacion() {
        return preparacion;
    }


    @Override
    public String toString() {
        return "ProductoRecord{" +
// TODO
                '}';
    }
}
