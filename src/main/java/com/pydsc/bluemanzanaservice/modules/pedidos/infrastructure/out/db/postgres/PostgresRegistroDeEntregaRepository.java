package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;



import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.RegistroDeEntregaRepository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PostgresRegistroDeEntregaRepository implements RegistroDeEntregaRepository {

    private final PostgresJpaRegistroDeEntregaRepository repository;
    private final PostgresRegistroDeEntregaNumeratorRepository numeratorRepository;
    private final EntityManager entityManager;

    public PostgresRegistroDeEntregaRepository(final PostgresJpaRegistroDeEntregaRepository repository,
    		PostgresRegistroDeEntregaNumeratorRepository numeratorRepository,
    		@Qualifier("blueManzanaEntityManager") final EntityManager entityManager) {
    	this.repository = repository;
    	this.numeratorRepository = numeratorRepository;
        this.entityManager = entityManager;
    }

    @Override
    public void save(final RegistroDeEntregaRecord registroDeEntrega) {
        repository.save(RegistroDeEntregaEntityMapper.registroDeEntregaRecordToRegistroDeEntregaEntity(registroDeEntrega));
    }


    @Override
    public Short findNextNumerator() {
        return numeratorRepository.findNextNumerator();
    }
}
