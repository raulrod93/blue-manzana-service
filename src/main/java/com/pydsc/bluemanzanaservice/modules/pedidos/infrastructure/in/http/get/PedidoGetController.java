package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Pedido.PATH_API_PEDIDOS_FINDER;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.PedidoFinder;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQuery;
import com.pydsc.bluemanzanaservice.modules.pedidos.application.finder.dto.PedidoFinderQueryResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto.PedidoFinderHttpResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto.PedidoHttpRequest;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.SuccessResponse;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(PATH_API_PEDIDOS)
@RequiredArgsConstructor
public class PedidoGetController {

    private final PedidoFinder pedidoFinder;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = PATH_API_PEDIDOS_FINDER, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse<PedidoFinderHttpResponse>> finder(
            final PedidoHttpRequest request) {

    		final PedidoFinderQueryResponse response = pedidoFinder.finder(
                    PedidoFinderQuery.builder()
                            .id(request.getId())
                            .build());

            return ResponseEntity.ok()
                    .body(new SuccessResponse<>(PedidoHttpMapper.queryResponseToHttpResponse(response)));
    }
}
