package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres;

import static com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.out.db.postgres.CustomPedidoRepositoryQueryBuilder.buildQuery;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoCriteria;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoIdRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRecord;
import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoRepository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PostgresPedidoRepository implements PedidoRepository {

    private final PostgresJpaPedidoRepository repository;
    private final EntityManager entityManager;

    public PostgresPedidoRepository(final PostgresJpaPedidoRepository repository,
    		@Qualifier("blueManzanaEntityManager") final EntityManager entityManager) {
    	this.repository = repository;
        this.entityManager = entityManager;
    }

    @Override
    public PedidoCriteriaResponse finder(final PedidoCriteria criteria) {

        List<Tuple> rows = buildQuery(criteria, entityManager).getResultList();
        List<PedidoDto> dtos = rows.isEmpty()
                ? Collections.emptyList() : PedidoEntityMapper.convertResultListToPedidoDtoList(rows);

        final List<PedidoRecord> records = dtos.stream()
                .map(PedidoEntityMapper::pedidoDtoToPedidoRecord)
                .collect(Collectors.toList());
        
        final PedidoRecord record = records.size() > 0 ? records.get(0) : null;

        return new PedidoCriteriaResponse(record);
    }
    

    @Override
    public Optional<PedidoRecord> findById(final PedidoIdRecord id) {
        final Optional<PedidoEntity> entity = repository.findById(id.getId());
        return entity.map(PedidoEntityMapper::pedidoEntityToPedidoRecord);
    }

    @Override
    public void save(final PedidoRecord pedido) {
        repository.save(PedidoEntityMapper.pedidoRecordToPedidoEntity(pedido));
    }
}
