package com.pydsc.bluemanzanaservice.modules.pedidos.infrastructure.in.http.get.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PedidoHttpRecord {
	
    private Short id;

    private Short cantidad;

    private Date fecha;

    private Short estado;

    private Short producto;

    private Short solicitadoen;

    private Short preparacion;
}
