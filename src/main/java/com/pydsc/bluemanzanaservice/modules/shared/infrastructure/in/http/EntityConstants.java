package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

public final class EntityConstants {

    private EntityConstants() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static class Tables {

        private Tables() {
            throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
        }

        public static final String PRODUCTO = "productos";
        public static final String EXISTENCIAS_EN_TIENDA = "existenciasentiendas";
        public static final String TIENDAS = "tiendas";
        public static final String PEDIDOS = "pedidosdesuministrodeproducto";
        public static final String REGISTROS_DE_ENTREGA = "registrodeentregas";
        public static final String USUARIOS = "usuarios";
    }

}
