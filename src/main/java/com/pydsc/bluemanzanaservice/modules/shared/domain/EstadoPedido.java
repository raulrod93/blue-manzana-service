package com.pydsc.bluemanzanaservice.modules.shared.domain;

import java.util.HashMap;
import java.util.Map;

public enum EstadoPedido {

	  PENDING("1"),
	  CONFIRMED("2"),
	  PREPARED("3"),
	  SENT("4"),
	  IN_DELIVERY("5"),
	  DELIVERED("6"),
	  RECLAIMED("7");

    private final String value;

    private static final Map<String, EstadoPedido> BY_VALUE = new HashMap<>();

    static {
        for (EstadoPedido idmt : values()) {
            BY_VALUE.put(idmt.value.toLowerCase(), idmt);
        }
    }

    EstadoPedido(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
