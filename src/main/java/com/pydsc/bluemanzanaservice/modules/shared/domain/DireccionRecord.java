package com.pydsc.bluemanzanaservice.modules.shared.domain;

public class DireccionRecord {

	private final Short id;
	
	private final String calle;
	
	private final Short numero;
	
	private final String otros;
	
	private final Short codigopostal;
	
	private final String localidad;
	
	private final String provincia;

    public static DireccionRecord.Builder newBuilder() {
        return new DireccionRecord.Builder();
    }

    private DireccionRecord(final Builder builder) {

        this.id = builder.id;
        this.calle = builder.calle;
        this.numero = builder.numero;
        this.otros = builder.otros;
        this.codigopostal = builder.codigopostal;
        this.localidad = builder.localidad;
        this.provincia = builder.provincia;
    }

    public static final class Builder {

    private Short id;
    private String calle;
    private Short numero;
    private String otros;
    private Short codigopostal;
    private String localidad;
    private String provincia;

        private Builder() {
            // Builder
        }

        public static Builder aTemperatureReportItem() {
            return new Builder();
        }
        public Builder id(Short id) {
            this.id = id;
            return this;
        }

        public Builder calle(String calle) {
            this.calle = calle;
            return this;
        }

        public Builder numero(Short numero) {
            this.numero = numero;
            return this;
        }

        public Builder otros(String otros) {
            this.otros = otros;
            return this;
        }

        public Builder codigopostal(Short codigopostal) {
            this.codigopostal = codigopostal;
            return this;
        }

        public Builder localidad(String localidad) {
            this.localidad = localidad;
            return this;
        }

        public Builder provincia(String provincia) {
            this.provincia = provincia;
            return this;
        }

        public DireccionRecord build() {
            return new DireccionRecord(this);
        }
    }

    public Short getId() {
        return id;
    }

    public String getCalle() {
        return calle;
    }

    public Short getNumero() {
        return numero;
    }

    public String getOtros() {
        return otros;
    }

    public Short getCodigopostal() {
        return codigopostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    @Override
    public String toString() {
        return "DireccionRecord{" +
                "id=" + id +
                ", calle=" + calle +
                ", numero=" + numero +
                ", otros=" + otros +
                ", codigopostal=" + codigopostal +
                ", localidad=" + localidad +
                ", provincia=" + provincia +
                '}';
    }
}
