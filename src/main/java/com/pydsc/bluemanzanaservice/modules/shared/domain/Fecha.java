package com.pydsc.bluemanzanaservice.modules.shared.domain;

public class Fecha {

	private final Integer dia;
	
	private final Integer mes;
	
	private final Integer anio;
	
	public Fecha(final Integer dia, final Integer mes, final Integer anio) {

        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }

    public Integer getDia() {
        return dia;
    }

    public Integer getMes() {
        return mes;
    }

    public Integer getAnio() {
        return anio;
    }
}
