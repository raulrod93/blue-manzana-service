package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SuccessResponse<T> implements ApiResponse {

    private final T data;

    private List<Error> errors;

}
