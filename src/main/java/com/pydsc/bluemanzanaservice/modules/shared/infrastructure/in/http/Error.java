package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error implements Serializable {

    private final String title;

    private String status;

    private List<String> details;

    private String code;

    private String operationId;

}
