package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse implements Serializable, ApiResponse {

    private final List<Error> errors;

}
