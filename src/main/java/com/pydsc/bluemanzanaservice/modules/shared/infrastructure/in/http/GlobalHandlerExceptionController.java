package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import com.pydsc.bluemanzanaservice.modules.pedidos.domain.PedidoNotFoundException;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalHandlerExceptionController {

    @ResponseBody
    @ExceptionHandler(value = {
            MethodArgumentNotValidException.class
    })
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(
            final MethodArgumentNotValidException exception) {

        final String errorLog = exception.getBindingResult().getAllErrors()
                .stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining(", "));
        log.error("MethodArgumentNotValidException: {}", errorLog);

        final List<String> errors = exception.getBindingResult().getAllErrors()
                .stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponseFactory.genericBadRequestError(errors));
    }

    @ResponseBody
    @ExceptionHandler(value = {
            BindException.class
    })
    public ResponseEntity<ErrorResponse> handleBindException(
            final BindException exception) {

        final String errorLog = exception.getBindingResult().getAllErrors()
                .stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining(", "));
        log.error("BindException: {}", errorLog);

        final List<String> errors = exception.getBindingResult().getAllErrors()
                .stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponseFactory.genericBadRequestError(errors));
    }

    @ResponseBody
    @ExceptionHandler(value = {
            HttpClientErrorException.class
    })
    public ResponseEntity<ErrorResponse> handleClientErrorException(final HttpClientErrorException exception) {

        log.error("ClientErrorException: {}", exception.getMessage());
        return ResponseEntity.status(exception.getStatusCode())
                .body(ApiResponseFactory.genericHttpClientError(exception));
    }

    @ResponseBody
    @ExceptionHandler(value = {
            ProductoNotFoundException.class,
            PedidoNotFoundException.class
    })
    public ResponseEntity<ErrorResponse> handleDomainServiceNotFoundException(final Exception exception) {

        log.error("DomainServiceNotFoundException: {}", exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ApiResponseFactory.genericNotFoundError(List.of(exception.getMessage()),
                        exception.getClass().getSimpleName()));
    }

    @ResponseBody
    @ExceptionHandler(value = {
            Exception.class
    })
    public ResponseEntity<ErrorResponse> handleGeneralException(final Exception exception) {

        log.error("GeneralException: {}", exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ApiResponseFactory.genericSimpleError());
    }
}