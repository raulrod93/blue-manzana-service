package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

public final class ApiResponseFactory {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private ApiResponseFactory() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    // Success response
    public static SuccessResponse<String> basicStringSuccessResponse(final String detail) {
        return new SuccessResponse<>(detail);
    }

    // Error response
    public static ErrorResponse genericSimpleError() {
        return getErrorResponse(List.of(INTERNAL_SERVER_ERROR.getReasonPhrase()), INTERNAL_SERVER_ERROR);
    }

    public static ErrorResponse genericBadRequestError(final List<String> details) {
        return getErrorResponse(details, BAD_REQUEST);
    }

    public static ErrorResponse genericNotFoundError(final List<String> details) {
        return getErrorResponse(details, NOT_FOUND);
    }

    public static ErrorResponse genericNotFoundError(final List<String> details, final String code) {
        return getErrorResponse(details, NOT_FOUND, code);
    }

    public static ErrorResponse genericConflictError(final List<String> details) {
        return getErrorResponse(details, CONFLICT);
    }

    public static ErrorResponse genericConflictError(final List<String> details, final String code) {
        return getErrorResponse(details, CONFLICT, code);
    }

    public static ErrorResponse genericForbiddenError(final List<String> details, final String code) {
        return getErrorResponse(details, FORBIDDEN, code);
    }

    public static ErrorResponse genericHttpClientError(final HttpClientErrorException exception) {

        TypeReference<HashMap<String, String>> typeRef = new TypeReference<>() {
        };

        try {
            Map<String, String> mapResponse = MAPPER.readValue(exception.getResponseBodyAsString(), typeRef);

            final var error = Error.builder()
                    .title(exception.getStatusCode().name())
                    .status(String.valueOf(exception.getRawStatusCode()))
                    .details(List.of(mapResponse.get("error_description")))
                    .build();

            return new ErrorResponse(List.of(error));

        } catch (final JsonProcessingException e) {
            return genericSimpleError();
        }
    }

    private static ErrorResponse getErrorResponse(final List<String> details, final HttpStatus status) {
        final var error = Error.builder()
                .title(status.name())
                .status(String.valueOf(status.value()))
                .details(details).build();

        return new ErrorResponse(List.of(error));
    }

    private static ErrorResponse getErrorResponse(final List<String> details, final HttpStatus status,
                                                  final String code) {
        final var error = Error.builder()
                .title(status.name())
                .status(String.valueOf(status.value()))
                .details(details)
                .code(code).build();

        return new ErrorResponse(List.of(error));
    }

}