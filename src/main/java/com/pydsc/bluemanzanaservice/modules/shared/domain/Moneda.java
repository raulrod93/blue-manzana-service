package com.pydsc.bluemanzanaservice.modules.shared.domain;

public final class Moneda {
	private Float valor;
	
	public Moneda(final Float valor) {

        this.valor = valor;
    }

    public Float getValor() {
        return valor;
    }

}
