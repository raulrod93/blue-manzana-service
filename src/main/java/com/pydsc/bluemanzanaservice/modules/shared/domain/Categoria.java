package com.pydsc.bluemanzanaservice.modules.shared.domain;

public final class Categoria {
	private String id;
	
	private String direccion;
	
	public Categoria(final String id, final String direccion) {

        this.id = id;
        this.direccion = direccion;
    }

    public String getId() {
        return id;
    }

    public String getDireccion() {
        return direccion;
    }

}
