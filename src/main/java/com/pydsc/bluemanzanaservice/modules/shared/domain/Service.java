package com.pydsc.bluemanzanaservice.modules.shared.domain;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Service {
    // Service Alias
}
