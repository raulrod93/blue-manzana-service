package com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http;

public class ApiConstants {

    private static final String UTILITY_CLASS = "This is a utility class and cannot be instantiated";
    private static final String PATH_SEPARATOR = "/";

    private ApiConstants() {
        throw new UnsupportedOperationException(UTILITY_CLASS);
    }

    public static class WebAppAPI {

        private WebAppAPI() {
            throw new UnsupportedOperationException(UTILITY_CLASS);
        }

        public static final String API_TAG_WEBAPP = "WebApp";
        public static final String API_TAG_WEBAPP_DESCRIPTION = "Blue Manzana - WebApp API";
        public static final String PATH_API_MAIN_WEBAPP = PATH_SEPARATOR + "api";

        public static class Producto {

            private Producto() {
                throw new UnsupportedOperationException(UTILITY_CLASS);
            }

            public static final String PATH_API_PRODUCTOS = PATH_API_MAIN_WEBAPP + PATH_SEPARATOR + "productos";

            public static final String PATH_API_PRODUCTOS_FILTER = PATH_SEPARATOR + "filter";
            
            public static final String PATH_API_PRODUCTOS_GET_BY_ID = PATH_SEPARATOR + "{productoId}";
        }

        public static class Tiendas {

            private Tiendas() {
                throw new UnsupportedOperationException(UTILITY_CLASS);
            }

            public static final String PATH_API_TIENDAS = PATH_API_MAIN_WEBAPP + PATH_SEPARATOR + "tiendas";

            public static final String PATH_API_TIENDAS_GET_PRODUCT_STOCK = PATH_SEPARATOR + "stock";
            
        }

        public static class Pedido {

            private Pedido() {
                throw new UnsupportedOperationException(UTILITY_CLASS);
            }

            public static final String PATH_API_PEDIDOS = PATH_API_MAIN_WEBAPP + PATH_SEPARATOR + "pedidos";

            public static final String PATH_API_PEDIDOS_FINDER = PATH_SEPARATOR + "find";

            public static final String PATH_API_PEDIDOS_CONFIRMAR_ENTREGA = PATH_SEPARATOR + "confirmar-entrega";
            
            public static final String PATH_API_PEDIDOS_CREAR_REGISTRO_DE_ENTREGA = PATH_SEPARATOR + "registro-de-entrega";
        }


    }

}
