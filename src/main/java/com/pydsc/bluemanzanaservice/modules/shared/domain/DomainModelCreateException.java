package com.pydsc.bluemanzanaservice.modules.shared.domain;

public class DomainModelCreateException extends RuntimeException {

    public DomainModelCreateException(final String message) {
        super(message);
    }
}