package com.pydsc.bluemanzanaservice.modules.productos.domain;

import java.util.Date;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

public class ProductoRecord {
	
	private final Short id;
	
	private final Short categoria;
	
	private final String categoriaDescripcion;
	
	private final String nombre;
	
	private final Float precio;
	
	private final Date fechaDeCreacion;

    public static ProductoRecord.Builder newBuilder() {
        return new ProductoRecord.Builder();
    }

    private ProductoRecord(final Builder builder) {

        this.id = builder.id;
        this.categoria = builder.categoria;
        this.categoriaDescripcion = builder.categoriaDescripcion;
        this.nombre = builder.nombre;
        this.precio = builder.precio;
        this.fechaDeCreacion = builder.fechaDeCreacion;
    }

    public static final class Builder {
    	
    	private Short id;
    	private Short categoria;
    	private String categoriaDescripcion;
    	private String nombre;
    	private Float precio;
    	private Date fechaDeCreacion;

        private Builder() {
            // Builder
        }

        public static Builder productoRecord() {
            return new Builder();
        }

        public Builder id(Short id) {
            this.id = id;
            return this;
        }

        public Builder categoria(Short categoria) {
            this.categoria = categoria;
            return this;
        }

        public Builder categoriaDescripcion(String categoriaDescripcion) {
            this.categoriaDescripcion = categoriaDescripcion;
            return this;
        }

        public Builder nombre(String nombre) {
            this.nombre = nombre;
            return this;
        }

        public Builder precio(Float precio) {
            this.precio = precio;
            return this;
        }

        public Builder fechaDeCreacion(Date fechaDeCreacion) {
            this.fechaDeCreacion = fechaDeCreacion;
            return this;
        }

        public ProductoRecord build() {
            return new ProductoRecord(this);
        }
    }

    public Short getId() {
        return id;
    }

    public Short getCategoria() {
        return categoria;
    }

    public String getCategoriaDescripcion() {
        return categoriaDescripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public Float getPrecio() {
        return precio;
    }

    public Date getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    @Override
    public String toString() {
        return "ProductoRecord{" +
                "id=" + id +
                ", categoria=" + categoria +
                ", categoriaDescripcion=" + categoriaDescripcion +
                ", nombre=" + nombre +
                ", precio=" + precio +
                ", fechaDeCreacion=" + fechaDeCreacion +
                '}';
    }
}
