package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductoFilterHttpResponse {

    private final ProductoHttpFilter filter;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final List<ProductoHttpRecord> records;

}
