package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresJpaProductoRepository extends JpaRepository<ProductoEntity, Short> {
    // PostgresJpaProductoRepository

}
