package com.pydsc.bluemanzanaservice.modules.productos.domain;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;

public final class ProductoCriteria {
	
    private final String id;
	
    private final Fecha fechaDeCreacion;
    
    private ProductoCriteria(final Builder builder) {

        id = builder.id;

        fechaDeCreacion = builder.fechaDeCreacion;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public Fecha getFechaDeCreacion() {
        return fechaDeCreacion;
    }
    
    public static final class Builder {

        private String id;

        private Fecha fechaDeCreacion;

        private Builder() {
            // Builder
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder fechaDeCreacion(Fecha val) {
        	fechaDeCreacion = val;
            return this;
        }

        public ProductoCriteria build() {
            return new ProductoCriteria(this);
        }

    }

}
