package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductoHttpFilter {
	
	private Fecha fechaDeCreacion;

}
