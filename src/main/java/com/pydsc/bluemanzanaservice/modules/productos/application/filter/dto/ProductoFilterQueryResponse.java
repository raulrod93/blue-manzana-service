package com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto;

import java.util.List;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductoFilterQueryResponse {

    private final ProductoFilterQuery filter;

    private final List<ProductoFilterRecord> records;

}
