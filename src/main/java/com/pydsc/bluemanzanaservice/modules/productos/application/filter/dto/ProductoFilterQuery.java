package com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ProductoFilterQuery {

    private Fecha fechaDeCreacion;
}
