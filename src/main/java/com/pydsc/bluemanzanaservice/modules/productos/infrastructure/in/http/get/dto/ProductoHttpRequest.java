package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;

import lombok.Data;

@Data
public class ProductoHttpRequest {
	
	private Fecha fechaDeCreacion;

}
