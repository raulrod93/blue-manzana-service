package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class ProductoIdEntity implements Serializable  {

    public ProductoIdEntity(ProductoIdEntity id2) {
		// TODO Auto-generated constructor stub
	}

	public ProductoIdEntity(String id2) {
		// TODO Auto-generated constructor stub
	}

	@Column(name = "id")
    private Short id;
}
