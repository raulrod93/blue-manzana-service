package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Producto.PATH_API_PRODUCTOS;
import static com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.ApiConstants.WebAppAPI.Producto.PATH_API_PRODUCTOS_FILTER;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pydsc.bluemanzanaservice.modules.productos.application.filter.ProductoFilter;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQuery;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto.ProductoFilterHttpResponse;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto.ProductoHttpRequest;
import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.SuccessResponse;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(PATH_API_PRODUCTOS)
@RequiredArgsConstructor
public class ProductoGetController {

    private final ProductoFilter productoFilter;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = PATH_API_PRODUCTOS_FILTER, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse<ProductoFilterHttpResponse>> filter(
            final ProductoHttpRequest request) {
    		final ProductoFilterQueryResponse response = productoFilter.filter(
                    ProductoFilterQuery.builder()
                            .fechaDeCreacion(request.getFechaDeCreacion())
                            .build());

            return ResponseEntity.ok()
                    .body(new SuccessResponse<>(ProductoHttpMapper.queryResponseToHttpResponse(response)));
    }
}
