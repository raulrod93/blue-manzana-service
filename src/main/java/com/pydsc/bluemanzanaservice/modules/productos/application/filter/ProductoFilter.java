package com.pydsc.bluemanzanaservice.modules.productos.application.filter;


import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQuery;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoIdRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoNotFoundException;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRepository;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoResponse;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres.ProductoIdEntity;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductoFilter {
    private final ProductoRepository productoRepository;

    public ProductoFilterQueryResponse filter(final ProductoFilterQuery query) {
        final var criteria = ProductoFilterMapper.queryToProductCriteria(query);
        final var product = productoRepository.filter(criteria);
        return ProductoFilterMapper.productRecordToQueryResponse(product);

    }
}
