package com.pydsc.bluemanzanaservice.modules.productos.domain;

public class ProductoNotFoundException extends RuntimeException {

    public ProductoNotFoundException(String message) {
        super(message);
    }

    public static ProductoNotFoundException ofId(final String id) {
        return new ProductoNotFoundException("Producto con [id] " + id + " no encontrado");
    }
}
