package com.pydsc.bluemanzanaservice.modules.productos.domain;

import java.util.Date;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

import lombok.Data;

@Data
public class ProductoResponse {
	
	private Short id;
	
	private Short categoria;
	
	private String categoriaDescripcion;
	
	private String nombre;
	
	private Float precio;
	
	private Date fechaDeCreacion;

}
