package com.pydsc.bluemanzanaservice.modules.productos.domain;

public class ProductoIdRecord {

    private final Short id;

    public ProductoIdRecord(final String id) {
        this.id = Short.valueOf(id);
    }

    public Short getId() {
        return id;
    }
}
