package com.pydsc.bluemanzanaservice.modules.productos.domain;

import java.util.Optional;

import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres.ProductoIdEntity;

public interface ProductoRepository {

	ProductoCriteriaResponse filter(ProductoCriteria criteria);
}
