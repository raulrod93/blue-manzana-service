package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoCriteria;

public class CustomProductoRepositoryQueryBuilder {

    private CustomProductoRepositoryQueryBuilder() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    private static final String Q_BASE =
            "SELECT pr.id AS id," +
					" pr.categoria AS categoria," +
            		" ca.descripcion as categoriaDescripcion," +
                    " pr.nombre AS nombre," +
                    " pr.precio AS precio," +
                    " pr.fechaDeCreacion AS fechaDeCreacion " +
                    " FROM productos pr" +
                    " LEFT JOIN categorias ca ON pr.categoria = ca.id";

    private static final String Q_FILTER_DATE_CREATION= " AND ca.fechaDeCreacion = :fechaDeCreacion";
    
    private static final String P_DATE_CREATION = "fechaDeCreacion";

    static Query buildQuery(final ProductoCriteria criteria, final EntityManager entityManager) {

        final var query = entityManager.createNativeQuery(createQuery(criteria), Tuple.class);
        setParams(query, criteria);

        return query;
    }

    private static String createQuery(final ProductoCriteria criteria) {

        final StringBuilder query;

        query = new StringBuilder(Q_BASE);
        if (criteria.getFechaDeCreacion() != null) query.append(Q_FILTER_DATE_CREATION);

        return query.toString();
    }

    private static void setParams(final Query query, final ProductoCriteria criteria) {

        if (criteria.getFechaDeCreacion() != null) query.setParameter(P_DATE_CREATION, criteria.getFechaDeCreacion());
    }
}
