package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import java.util.Date;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ProductoDto {
	
	private final Short id;
	
	private final Short categoria;
	
	private final String categoriaDescripcion;
	
	private final String nombre;
	
	private final Float precio;
	
	private final Date fechaDeCreacion;
}
