package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

public final class ProductoEntityMapper {

    private ProductoEntityMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static List<ProductoDto> convertResultListToProductoDtoList(final List<Tuple> tuples) {

        return tuples.stream()
                .map(ProductoEntityMapper::convertRowToProductoDto)
                .collect(Collectors.toList());
    }

    private static ProductoDto convertRowToProductoDto(final Tuple tuple) {

        return ProductoDto.builder()
        		.id(tuple.get("id", Short.class))
        		.categoria(tuple.get("categoria", Short.class))
        		.categoriaDescripcion(tuple.get("categoriaDescripcion", String.class))
        		.nombre(tuple.get("nombre", String.class))
        		.precio(tuple.get("precio", Float.class))
        		.fechaDeCreacion(tuple.get("fechaDeCreacion", Date.class))
                .build();
    }

    static ProductoRecord productoDtoToProductoRecord(final ProductoDto input) {

        return ProductoRecord.newBuilder()
                .id(input.getId())
                .categoria(input.getCategoria())
                .categoriaDescripcion(input.getCategoriaDescripcion())
                .nombre(input.getNombre())
                .precio(input.getPrecio())
                .fechaDeCreacion(input.getFechaDeCreacion())
                .build();
    }

    static ProductoRecord productoEntityToProductoRecord(final ProductoEntity input) {

        return ProductoRecord.newBuilder()
                .id(input.getId())
                .categoria(input.getCategoria())
                .categoriaDescripcion(input.getCategoriaDescripcion())
                .nombre(input.getNombre())
                .precio(input.getPrecio())
                .fechaDeCreacion(input.getFechaDeCreacion())
                .build();
    }
}
