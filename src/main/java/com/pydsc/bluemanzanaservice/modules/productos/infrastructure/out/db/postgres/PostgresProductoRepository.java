package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import static com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres.CustomProductoRepositoryQueryBuilder.buildQuery;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoCriteria;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoIdRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRepository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PostgresProductoRepository implements ProductoRepository {

    private final PostgresJpaProductoRepository repository;
    private final EntityManager entityManager;

    public PostgresProductoRepository(final PostgresJpaProductoRepository repository,
    		@Qualifier("blueManzanaEntityManager") final EntityManager entityManager) {
    	this.repository = repository;
        this.entityManager = entityManager;
    }

    @Override
    public ProductoCriteriaResponse filter(final ProductoCriteria criteria) {

        List<Tuple> rows = buildQuery(criteria, entityManager).getResultList();
        List<ProductoDto> dtos = rows.isEmpty()
                ? Collections.emptyList() : ProductoEntityMapper.convertResultListToProductoDtoList(rows);

        final List<ProductoRecord> records = dtos.stream()
                .map(ProductoEntityMapper::productoDtoToProductoRecord)
                .collect(Collectors.toList());

        return new ProductoCriteriaResponse(criteria, records);
    }
}
