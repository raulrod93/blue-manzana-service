package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.out.db.postgres;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pydsc.bluemanzanaservice.modules.shared.infrastructure.in.http.EntityConstants;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = EntityConstants.Tables.PRODUCTO)
public class ProductoEntity {

    @Id
    @Column(name = "id")
    private Short id;

    @Column(name = "categoria", nullable = false)
    private Short categoria;

   
    @Column(name = "categoriaDescripcion", nullable = true)
    @Basic(optional=true)
    private String categoriaDescripcion;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "precio", nullable = false)
    private Float precio;

    @Column(name = "fechaDeCreacion", nullable = false)
    private Date fechaDeCreacion;

    @Override
    public String toString() {
        return "ProductoEntity{" +
                "id=" + id +
                ", categoria='" + categoria + '\'' +
                ", categoriaDescripcion='" + categoriaDescripcion + '\'' +
                ", nombre='" + nombre + '\'' +
                ", precio='" + precio + '\'' +
                ", fechaDeCreacion=" + fechaDeCreacion +
                '}';
    }
}
