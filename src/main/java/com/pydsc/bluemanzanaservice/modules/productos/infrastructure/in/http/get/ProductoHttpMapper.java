package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get;

import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto.ProductoHttpFilter;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto.ProductoHttpRecord;

import java.util.stream.Collectors;

import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQuery;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoResponse;
import com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto.ProductoFilterHttpResponse;

final class ProductoHttpMapper {

    private ProductoHttpMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static ProductoHttpFilter queryFilterToTHttpFilter(final ProductoFilterQuery filter) {

        return ProductoHttpFilter.builder()
                .fechaDeCreacion(filter.getFechaDeCreacion())
                .build();
    }

    static ProductoHttpRecord queryRecordToHttpRecord(final ProductoFilterRecord reportFilterRecord) {

        return ProductoHttpRecord.builder()
        		.id(reportFilterRecord.getId())
        		.categoria(reportFilterRecord.getCategoria())
        		.categoriaDescripcion(reportFilterRecord.getCategoriaDescripcion())
        		.nombre(reportFilterRecord.getNombre())
        		.precio(reportFilterRecord.getPrecio())
                .fechaDeCreacion(reportFilterRecord.getFechaDeCreacion())
                .build();
    }

    static ProductoFilterHttpResponse queryResponseToHttpResponse(
            final ProductoFilterQueryResponse response) {

        final var httpFilter = queryFilterToTHttpFilter(response.getFilter());

        final var httpRecords = response.getRecords().stream()
                .map(ProductoHttpMapper::queryRecordToHttpRecord).collect(Collectors.toList());

        return new ProductoFilterHttpResponse(httpFilter, httpRecords);
    }

}
