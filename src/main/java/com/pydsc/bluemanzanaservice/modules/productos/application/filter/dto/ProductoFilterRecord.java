package com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto;

import java.util.Date;

import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class ProductoFilterRecord {
	
	private Short id;
	
	private Short categoria;
	
	private String categoriaDescripcion;
	
	private String nombre;
	
	private Float precio;
	
	private Date fechaDeCreacion;

}
