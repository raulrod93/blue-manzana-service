package com.pydsc.bluemanzanaservice.modules.productos.domain;

import java.util.List;

public class ProductoCriteriaResponse {

    private ProductoCriteria filter;


    private final List<ProductoRecord> records;

    public ProductoCriteriaResponse(final ProductoCriteria filter, final List<ProductoRecord> records) {

        this.filter = filter;
        this.records = records;
    }

    public ProductoCriteria getFilter() {
        return filter;
    }


    public List<ProductoRecord> getRecords() {
        return records;
    }

    @Override
    public String toString() {
        return "ProductoCriteriaResponse{" +
                "filter=" + filter +
                ", records=" + records +
                '}';
    }

}
