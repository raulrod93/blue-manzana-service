package com.pydsc.bluemanzanaservice.modules.productos.infrastructure.in.http.get.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Fecha;
import com.pydsc.bluemanzanaservice.modules.shared.domain.Moneda;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductoHttpRecord {

	private Short id;
	
	private Short categoria;
	
	private String categoriaDescripcion;
	
	private String nombre;
	
	private Float precio;
	
	private Date fechaDeCreacion;
}
