package com.pydsc.bluemanzanaservice.modules.productos.application.filter;

import java.util.Optional;
import java.util.stream.Collectors;

import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQuery;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterQueryResponse;
import com.pydsc.bluemanzanaservice.modules.productos.application.filter.dto.ProductoFilterRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoCriteria;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoCriteriaResponse;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoRecord;
import com.pydsc.bluemanzanaservice.modules.productos.domain.ProductoResponse;

final class ProductoFilterMapper {

    private ProductoFilterMapper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }


    public static ProductoCriteria queryToProductCriteria(final ProductoFilterQuery query) {
        return ProductoCriteria.newBuilder()
        		.fechaDeCreacion(query.getFechaDeCreacion())
                .build();
    }

    static ProductoFilterQueryResponse productRecordToQueryResponse(
            final ProductoCriteriaResponse productoCriteriaResponse) {

        final var queryFilter = criteriaToQuery(productoCriteriaResponse.getFilter());

        final var queryRecords = productoCriteriaResponse.getRecords().stream()
                .map(ProductoFilterMapper::productoRecordToResponse).collect(Collectors.toList());

        return new ProductoFilterQueryResponse(queryFilter, queryRecords);
    }

    public static ProductoResponse productRecordToResponse(final ProductoRecord produto) {

        final var output = new ProductoResponse();

        output.setId(produto.getId());
        output.setCategoria(produto.getCategoria());
        output.setNombre(produto.getNombre());
        output.setPrecio(produto.getPrecio());
        output.setFechaDeCreacion(produto.getFechaDeCreacion());

        return output;
    }
    static ProductoFilterRecord productoRecordToResponse(final ProductoRecord productoRecord) {

        return baseBuilder(productoRecord).build();
    }


    static ProductoFilterQuery criteriaToQuery(final ProductoCriteria criteria) {
        return ProductoFilterQuery.builder()
        		.fechaDeCreacion(criteria.getFechaDeCreacion())
                .build();
    }

    private static ProductoFilterRecord.Builder baseBuilder(final ProductoRecord productoRecord) {

        return ProductoFilterRecord.builder()
        		.id(productoRecord.getId())
        		.categoria(productoRecord.getCategoria())
        		.categoriaDescripcion(productoRecord.getCategoriaDescripcion())
        		.nombre(productoRecord.getNombre())
        		.precio(productoRecord.getPrecio())
        		.fechaDeCreacion(productoRecord.getFechaDeCreacion());
    }
}
